const http = require('http')
const port = 8080

http.createServer((req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/html'
  })
  res.write('<h1>Welcome to your first app on GitLab!</h1>')
  res.end()
}).listen(port, () => {
  console.log('Server is running on port ' + port)
})